//Obtendo a conexão com banco de dados
const connect = require("../db/connect");

module.exports = class clienteController {
  static async createCliente(req, res) {
    const {
      telefone,
      nome,
      cpf,
      logradouro,
      numero,
      complemento,
      bairro,
      cidade,
      estado,
      cep,
      referencia,
    } = req.body;

    //vendo se o atributo chave é diferente de 0 (zero)
    if (telefone !== 0) {
      const query = `insert into cliente (telefone, nome, cpf, logradouro,numero, complemento, bairro, cidade, estado, cep, referencia) 
            values ('${telefone}', '${nome}', '${cpf}', '${logradouro}', '${numero}', '${complemento}', '${bairro}', '${cidade}', '${estado}', '${cep}', '${referencia}')`;

      try {
        connect.query(query, function (err) {
          if (err) {
            console.log(err);
            res
              .status(500)
              .json({ error: "Usuário não cadastrado no banco!!!" });
            return;
          }

          console.log("Inserindo no Banco!!!");
          res.status(201).json({ message: "Usuário criado com sucesso!!!" });
        });
      } catch (error) {
        console.error("Erro ao executar insert!!! - ", error);
        res.status(500).json({ error: "Erro interno do servidor!!!" });
      }
    } 
    
    else {
      res.status(400).json({ message: "O telefone é obrigatório!!!" });
    }
  }

  //select da tabela cliente
  static async getAllClientes(req, res){
    const query = `select * from cliente`;

    try{
      connect.query(query, function(err, data){
        if(err){
          console.log(err);
          res.status(500).json({error: "Usuários não encontrados no banco!!"});
          return;
        }
        let clientes = data;
        console.log("Consulta realizada com sucesso!");
        res.status(201).json({clientes});
      });

    }
    catch(error){
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({error: "Erro interno do servidor!!!"});
    }
  }

  static async getAllClientesTwo(req, res){
    const query = `select * from cliente`;
  }
};
