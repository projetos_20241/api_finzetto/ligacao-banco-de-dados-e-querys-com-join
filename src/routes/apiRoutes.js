const dbController = require('../controller/dbController')
const clienteController = require('../controller/clienteController')
const pizzariaController = require('../controller/pizzariaController')
const router = require('express').Router()

router.get("/tables", dbController.getTables)
router.get("/tablesDescription", dbController.getTablesDescriptions)

router.post("/cliente", clienteController.createCliente)
router.post("/cliente", clienteController.getAllClientes)

router.get("/pizza", pizzariaController.listarPedidosPizzas)
router.get("/listarPedidosPizzasComJoin", pizzariaController.listarPedidosPizzasComJoin)
router.get("/listar", pizzariaController.listar)
module.exports = router